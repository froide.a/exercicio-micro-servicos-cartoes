package com.Microservico.cartao.DTO;

import com.Microservico.cartao.model.Cartao;

public class CartaoResponse {
    private long id;
    private long numero;
    private long clienteId;
    private boolean ativo;

    public CartaoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }


    public CartaoResponse converterParaCadastroCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getClienteId());
        cartaoResponse.setAtivo(cartao.isAtivo());
        return cartaoResponse;
    }


}
