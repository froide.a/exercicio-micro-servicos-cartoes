package com.Microservico.cartao.DTO;

public class SituacaoCartaoRequest {


    private boolean ativo;

    public SituacaoCartaoRequest() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
