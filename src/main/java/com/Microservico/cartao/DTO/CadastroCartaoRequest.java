package com.Microservico.cartao.DTO;

import com.Microservico.cartao.model.Cartao;

import javax.validation.constraints.NotNull;

public class CadastroCartaoRequest {
    @NotNull(message = "Informe o número do cartão.")
    private long numero;

    @NotNull(message = "Informe o Id do cliente.")
    private long clienteId;

    public CadastroCartaoRequest() {
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }


    public Cartao converterParaCartao(CadastroCartaoRequest cadastroCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cadastroCartaoRequest.getNumero());
        return cartao;
    }


}
