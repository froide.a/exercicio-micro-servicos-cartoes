package com.Microservico.cartao.controller;

import com.Microservico.cartao.DTO.CadastroCartaoRequest;
import com.Microservico.cartao.DTO.CartaoResponse;
import com.Microservico.cartao.DTO.SituacaoCartaoRequest;
import com.Microservico.cartao.model.Cartao;
import com.Microservico.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse cadastrarCartao (@RequestBody @Valid CadastroCartaoRequest cadastroCartaoRequest) {
        try {
             CartaoResponse cartaoResponse = new CartaoResponse();
             Cartao cartao = cartaoService.cadastrarCartao(cadastroCartaoRequest.getClienteId(),
                    cadastroCartaoRequest.converterParaCartao(cadastroCartaoRequest));
            return  cartaoResponse.converterParaCadastroCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @PatchMapping("/{id}")
    public CartaoResponse atualizarSituacaoCartao (@PathVariable(name = "id") long id, @RequestBody @Valid SituacaoCartaoRequest situacaoCartaoRequest) {
        try {
            CartaoResponse cartaoResponse = new CartaoResponse();
            Cartao cartao = cartaoService.atualizarSituacaoCartao(id, situacaoCartaoRequest.isAtivo());
            return cartaoResponse.converterParaCadastroCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CartaoResponse consultarCartaoPorId(@PathVariable(name = "id") long id) {

            CartaoResponse cartaoResponse = new CartaoResponse();
            Cartao cartao = cartaoService.consultarCartaoPorId(id);
            return cartaoResponse.converterParaCadastroCartaoResponse(cartao);

        }


}
