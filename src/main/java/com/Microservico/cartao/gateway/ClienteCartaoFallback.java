package com.Microservico.cartao.gateway;

public class ClienteCartaoFallback implements ClienteCartao {

    @Override
    public Cliente getByclienteId(Long clienteId) {
        Cliente cliente = new Cliente();
        cliente.setNome("Super Froide - Teste Fallback");
        return null;
    }
}
