package com.Microservico.cartao.gateway;

import javax.validation.constraints.NotNull;

public class Cliente {

    private long clienteId;
    private String nome;

    public Cliente() {
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
