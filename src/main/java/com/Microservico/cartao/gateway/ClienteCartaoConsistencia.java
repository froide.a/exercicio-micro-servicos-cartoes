package com.Microservico.cartao.gateway;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não cadastrado - tratamento de exception Consistencia .")
public class ClienteCartaoConsistencia extends RuntimeException {

}