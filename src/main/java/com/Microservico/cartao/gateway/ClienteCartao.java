package com.Microservico.cartao.gateway;

import com.Microservico.cartao.model.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface ClienteCartao {

    @GetMapping("/cliente/{id}")
    Cliente getByclienteId(@PathVariable(name = "id") Long clienteId);
}


