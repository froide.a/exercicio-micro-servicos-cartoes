package com.Microservico.cartao.gateway;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteCartaoConfiguration {

    @Bean
    public ErrorDecoder getClienteCartaoDecoder() {
        return new ClienteCartaoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteCartaoFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);

    }
}

