package com.Microservico.cartao.gateway;

import com.Microservico.cartao.gateway.ClienteCartaoConsistencia;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCartaoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();


    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteCartaoConsistencia();
        }
        return errorDecoder.decode(s, response);
    }

    }
