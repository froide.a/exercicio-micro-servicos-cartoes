package com.Microservico.cartao;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    @Bean
    private IRule getRule() {
        return new RandomRule();
    }
}
