package com.Microservico.cartao.service;

import com.Microservico.cartao.DTO.CadastroCartaoRequest;
import com.Microservico.cartao.DTO.CartaoResponse;
import com.Microservico.cartao.gateway.Cliente;
import com.Microservico.cartao.gateway.ClienteCartao;
import com.Microservico.cartao.gateway.ClienteCartaoConsistencia;
import com.Microservico.cartao.model.Cartao;
import com.Microservico.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteCartao clienteCartao;

    public Cartao cadastrarCartao(long clienteId, Cartao cartao) {
        try {

                Cliente cliente = clienteCartao.getByclienteId(clienteId);
                cartao.setClienteId(cliente.getClienteId());
                cartao.setAtivo(false);

                return cartaoRepository.save(cartao);

        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o cartão: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }

    public Cartao consultarCartaoPorId(long id) {

            Optional<Cartao> cartao = cartaoRepository.findById(id);
            if(cartao.isPresent()) {
                return cartao.get();
            } else {
                throw new ClienteCartaoConsistencia();
            }

    }


    public Cartao atualizarSituacaoCartao(long id, boolean situacao) {
        try {
            Cartao cartao = consultarCartaoPorId(id);
            cartao.setAtivo(situacao);
            return cartaoRepository.save(cartao);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao atualizar o cartão: " +
                    e.getMessage());
        }
    }

}
