package com.Microservico.cartao.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cartao")
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "numero", nullable = false, unique = true)
    private long numero;

    @Column(name = "ativo", nullable = false)
    private boolean ativo;

    @Column(name = "ClienteId", nullable = false)
    private long ClienteId;

    public Cartao() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public long getClienteId() {
        return ClienteId;
    }

    public void setClienteId(long clienteId) {
        ClienteId = clienteId;
    }
}

